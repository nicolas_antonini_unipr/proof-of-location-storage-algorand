# PoL Storage

This repository contains the smart contract that allows to store Proofs of Location on the Algorand Blockchain and the necessary code to test it on the Algorand Sandbox.

## Requirements

In order to publish and fund the smart contract, the [Algorand Sandbox](https://github.com/algorand/sandbox) must be running. The Sandbox requires [Docker](https://docs.docker.com/) with Docker Compose in order to run.

Python 3.6 or higher must be installed.

The following python packages are required:

- `pyteal==0.9.1`

- `py-algorand-sdk==1.8.0`

- `mypy==0.910`

- `pytest`

- `black==21.7b0`


## Usage

The script can be run with the command:

`python3 algosc.py` 
