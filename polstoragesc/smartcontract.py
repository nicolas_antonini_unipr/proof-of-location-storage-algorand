from pyteal import *


def approval_program():
    creator_key = Bytes("creator")
    verifier_public_key_key = Bytes("ver_pub_key")
    creation_time_key = Bytes("creation_time")
    close_time_key = Bytes("close_time")
    last_pseudonym_key = Bytes("pseudonym")
    last_latitude_key = Bytes("last_lat")
    last_longitude_key = Bytes("last_lon")
    last_timestamp_key = Bytes("last_ts")
    last_signature_key = Bytes("last_sign")

    @Subroutine(TealType.none)
    def closeAccountTo(account: Expr) -> Expr:
        return If(Balance(Global.current_application_address()) != Int(0)).Then(
            Seq(
                InnerTxnBuilder.Begin(),
                InnerTxnBuilder.SetFields(
                    {
                        TxnField.type_enum: TxnType.Payment,
                        TxnField.close_remainder_to: account,
                    }
                ),
                InnerTxnBuilder.Submit(),
            )
        )

    #Handles the creation of the smart contract
    on_create_start_time = Btoi(Txn.application_args[1])
    on_create_end_time = Btoi(Txn.application_args[2])
    
    on_create = Seq(
        #Sets every global variable
        App.globalPut(creator_key, Txn.application_args[0]),
        App.globalPut(verifier_public_key_key, Bytes("")),
        App.globalPut(creation_time_key, on_create_start_time),
        App.globalPut(close_time_key, on_create_end_time),
        App.globalPut(last_pseudonym_key, Bytes("")),
        App.globalPut(last_latitude_key, Bytes("")),
        App.globalPut(last_longitude_key, Bytes("")),
        App.globalPut(last_timestamp_key, Bytes("")),
        App.globalPut(last_signature_key, Bytes("")),
        Assert(
            And(
                #Checks if the creation and stop time are correct
                Global.latest_timestamp() < on_create_start_time,
                on_create_start_time < on_create_end_time,
            )
        ),
        Approve(),
    )

    #Handles the setup of the smart contract
    on_setup = Seq(
        #Checks if the smart contract is active
        Assert(Global.latest_timestamp() > App.globalGet(creation_time_key)),

        #Checks that the smart contract has not been already started
        Assert(App.globalGet(verifier_public_key_key) == Bytes("") ),

        #Sets the public key of the verifier
        App.globalPut(verifier_public_key_key, Txn.application_args[1]),
        Approve(),
    )

    #Handles the post of a Proof of Location
    on_post_pol = Seq(
        Assert(
            And(
                #Check if the smart contract is active
                App.globalGet(creation_time_key) <= Global.latest_timestamp(),
                Global.latest_timestamp() < App.globalGet(close_time_key),

                #Checks if the smart contract has been setup
                App.globalGet(verifier_public_key_key) != Bytes("")
            )
        ),
        #Puts the data of the last Proof of Location
        App.globalPut(last_pseudonym_key, Txn.application_args[1]),
        App.globalPut(last_latitude_key, Txn.application_args[2]),
        App.globalPut(last_longitude_key, Txn.application_args[3]),
        App.globalPut(last_timestamp_key, Txn.application_args[4]),
        App.globalPut(last_signature_key, Txn.application_args[5]),
        Approve(),
    )

    on_call_method = Txn.application_args[0]
    on_call = Cond(
        [on_call_method == Bytes("setup"), on_setup],
        [on_call_method == Bytes("post_pol"), on_post_pol],
    )

    #Handles the delete of the smart contract, refunding the creator
    on_delete = Seq(
            #Clears the global state of the smart contract
            App.globalPut(verifier_public_key_key, Bytes("")),
            App.globalPut(last_pseudonym_key, Bytes("")),
            App.globalPut(last_latitude_key, Bytes("")),
            App.globalPut(last_longitude_key, Bytes("")),
            App.globalPut(last_timestamp_key, Bytes("")),
            App.globalPut(last_signature_key, Bytes("")),

            #Refunds the creator
            closeAccountTo(App.globalGet(creator_key)),
            Approve(),
        )
    

    #Checks the type of call to the smart contract
    program = Cond(
        #Creates the smart contract
        [Txn.application_id() == Int(0), on_create],
        
        #Handles a generic call
        [Txn.on_completion() == OnComplete.NoOp, on_call],
        [
            #Deletes the smart contract
            Txn.on_completion() == OnComplete.DeleteApplication,
            on_delete,
        ],
        [
            #The smart contract do not support calls of type OptIn, CloseOut and UpdateApplication.
            Or(
                Txn.on_completion() == OnComplete.OptIn,
                Txn.on_completion() == OnComplete.CloseOut,
                Txn.on_completion() == OnComplete.UpdateApplication,
            ),
            Reject(),
        ],
    )

    return program


def clear_state_program():
    return Approve()


