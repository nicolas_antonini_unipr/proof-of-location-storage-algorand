from time import time
from algosdk.logic import get_application_address
from polstoragesc.operations import createProofOfLocationDApp, fundProofOfLocationDApp
from polstoragesc.testing.setup import getAlgodClient
from polstoragesc.testing.resources import getTemporaryAccount

#Compiles, publishes and funds a smart contract for the storage of Proofs of Location
def publish_and_fund_smart_contract():

    #Gets an algod client connected to the sandbox
    print("Getting an algod client")
    client = getAlgodClient()

    #Gets a temporary account that will create and fund the smart contract
    print("Getting a temporary account")
    creator = getTemporaryAccount(client)

    #Creates the validity time of the smart contract (3 days)
    startTime = int(time()) + 60  
    endTime = startTime + 259_200   

    #Creates the smart contract
    print("Creating the smart contract")
    appID = createProofOfLocationDApp(
        client=client,
        sender=creator,
        startTime=startTime,
        endTime=endTime,
    )

    print("Smart contract created with id: ", appID, " with address: ", get_application_address(appID))

    print("Funding the smart contract")
    fundProofOfLocationDApp(
        client=client,
        appID=appID,
        funder=creator
    )

publish_and_fund_smart_contract()
